const mongoose = require('mongoose')
const Event = require('../models/event')
mongoose.connect('mongodb://localhost:27017/example')

async function clear () {
  await Event.deleteMany({})
}

async function main () {
  await clear()
  Event.insertMany([
    {
      title: 'Title 1', content: 'Content 1', startDate: new Date('2022-01-28 08:00'), endDate: new Date('2022-01-28 16:00'), class: 'a'
    },
    {
      title: 'Title 2', content: 'Content 2', startDate: new Date('2022-01-29 08:00'), endDate: new Date('2022-01-29 16:00'), class: 'b'
    },
    {
      title: 'Title 3', content: 'Content 3', startDate: new Date('2022-01-30 08:00'), endDate: new Date('2022-01-30 16:00'), class: 'c'
    },
    {
      title: 'Title 4', content: 'Content 4', startDate: new Date('2022-01-31 08:00'), endDate: new Date('2022-01-31 16:00'), class: 'b'
    },
    {
      title: 'Title 5', content: 'Content 5', startDate: new Date('2022-01-31 13:00'), endDate: new Date('2022-01-31 16:00'), class: 'c'
    }
  ])
  Event.find({})
}
main().then(function () {
  console.log('Finish')
})
